# Kicad6 Libraries

Persistant repo to house a personal selection of footprints and symbols for use across projects and courses.

#Using this repo:
1. Add it as a submodule to your new PCB project repo:
 - cd to the location in your new project that you want this to live
 - git submodule add git@gitlab.com:r4space/kicad6-libraries.git
2. Configure the submodule:
 - cd into new submodule directory
 - git checkout main //This is necessary as otherwise your submodule will exist as a detatched head (fine if you never want to update it but generally 
that's unwanted)
3. Tell your project repo that it should track the main branch and that when updating it should merge new commits from the remote in to the local:
 - Open .gitmodules in the main repo root
 - Edit the file to look something like the following (add the branch and update lines):
 ```
 [submodule "PCB/Library/kicad6-libraries"]
   path = PCB/Library/kicad6-libraries
   url = https://gitlab.com/r4space/kicad6-libraries.git
   branch = main
   update = merge 
```
4. In future to update the submodule run the following in the primary repo root:
```bash
git submodule update --remote
```

# Sources
Some suggestions for finding components
* [https://www.snapeda.com](https://www.snapeda.com)
* [https://componentsearchengine.com/](https://componentsearchengine.com/)
* For searching JLCPCB stock for available parts specifically: [https://yaqwsx.github.io/jlcparts/#/](https://yaqwsx.github.io/jlcparts/#/)

# License
[Creative Commons Attribution 4.0 International license](https://choosealicense.com/licenses/cc-by-4.0/)
